python-openstacksdk (1.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed munch from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 10:02:38 +0100

python-openstacksdk (0.101.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 11:19:29 +0200

python-openstacksdk (0.101.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.
  * Blacklist test_stats.TestStats.test_servers_error.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Jul 2022 09:48:19 +0200

python-openstacksdk (0.61.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 12:16:24 +0100

python-openstacksdk (0.61.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Remove_misspelled_speccing_arguments.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Feb 2022 17:24:20 +0100

python-openstacksdk (0.59.0-3) unstable; urgency=medium

  * Add Remove_misspelled_speccing_arguments.patch (Closes: #1002448).

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Dec 2021 14:41:23 +0100

python-openstacksdk (0.59.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:45:04 +0200

python-openstacksdk (0.59.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Aug 2021 08:59:27 +0200

python-openstacksdk (0.58.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-pygments from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Aug 2021 10:52:01 +0200

python-openstacksdk (0.55.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 10:37:04 +0200

python-openstacksdk (0.55.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Mar 2021 15:14:42 +0100

python-openstacksdk (0.50.0-6) unstable; urgency=medium

  * Added Restrictions: superficial to d/tests/control (Closes: #974516).

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Nov 2020 00:01:41 +0100

python-openstacksdk (0.50.0-5) unstable; urgency=medium

  * Fix again the autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 13:43:54 +0200

python-openstacksdk (0.50.0-4) unstable; urgency=medium

  * Fix the autopkgtest to import openstack, not openstacksdk.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 13:14:53 +0200

python-openstacksdk (0.50.0-3) unstable; urgency=medium

  * Fix autopkgtest to work with all supported Python 3 versions.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 12:11:06 +0200

python-openstacksdk (0.50.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.
  * Blacklist test_update_security_group_bad_kwarg() and
    test_create_port_parameters() which are time based tests that aren't
    deterministic.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 10:57:04 +0200

python-openstacksdk (0.50.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 21:43:01 +0200

python-openstacksdk (0.49.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 11:51:51 +0200

python-openstacksdk (0.48.0-1) experimental; urgency=medium

  * Fixed Homepage field.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Blacklist test__load_yaml_json_file_without_perm() that fails.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 15:51:25 +0200

python-openstacksdk (0.46.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 16:14:03 +0200

python-openstacksdk (0.46.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Run unit tests using debian/tmp folder.
  * Blacklist failing using test related to baremetal:
    - TestWaitForNodesProvisionState.test_timeout_and_failures_not_fail().

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 22:13:03 +0200

python-openstacksdk (0.36.2-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Mar 2020 12:43:03 +0100

python-openstacksdk (0.35.0-4) unstable; urgency=medium

  * Removed Testsuite: autopkgtest-pkg-python as it's trying to import
    "openstacksdk" instead of "openstack". Replaced it with custom tests doing
    the same thing, but in the correct way.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Mar 2020 23:59:21 +0100

python-openstacksdk (0.35.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 09:33:54 +0200

python-openstacksdk (0.35.0-2) experimental; urgency=medium

  * Re-enable testing at build time, and add some autopkgtest, thanks to
    Corey Bryant (Closes: #930086).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2019 17:30:55 +0200

python-openstacksdk (0.35.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 22:49:18 +0200

python-openstacksdk (0.27.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed version of keystoneauth1.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Jun 2019 14:32:29 +0200

python-openstacksdk (0.26.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Removed fix-prefixlen-creating-subnet.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 22:52:20 +0100

python-openstacksdk (0.17.2-3) unstable; urgency=medium

  * Add fix-prefixlen-creating-subnet.patch (Closes: #924376).
  * d/copyright: Add me to copyright
  * d/control: Add me to uploaders field

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 14 Mar 2019 18:54:10 +0100

python-openstacksdk (0.17.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 22:29:53 +0200

python-openstacksdk (0.17.2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Disable unit testing, taking a way too much resources.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Aug 2018 20:56:15 +0200

python-openstacksdk (0.11.3-3) unstable; urgency=medium

  * Fixed Python 3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Mar 2018 18:46:30 +0000

python-openstacksdk (0.11.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:32 +0000

python-openstacksdk (0.11.3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed intersphinx patch.
  * Switch doc to use Python 3: Py2 doesn't have itertools.filterfalse() that
    the doc is using.
  * Add install-missing-files.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 07:26:41 +0000

python-openstacksdk (0.9.17-2) unstable; urgency=medium

  * Set minimum version of openstackdocstheme (Closes: #880178).
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Oct 2017 22:44:59 +0000

python-openstacksdk (0.9.17-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Removed UPSTREAM_GIT with default value
  * d/copyright: Changed source URL to new one

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{test,install}.
  * Rebase remove-intersphinx.patch.
  * Remove remove-openstackdocstheme.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Oct 2017 15:10:42 +0200

python-openstacksdk (0.8.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 11:31:25 +0000

python-openstacksdk (0.8.1-1) experimental; urgency=medium

  * New upstream release.
  * Bumped required python-iso8601 version in build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Mar 2016 17:14:42 +0100

python-openstacksdk (0.8.0-1) experimental; urgency=medium

  [ Ivan Udovichenko ]
  * d/conrol: Update dependencies.
  * d/patches: Update patch remove-intersphinx.patch

  [ Corey Bryant ]
  * d/control: Update min version of python(3)-keystoneauth1 to ensure this
    bug is fixed: https://bugs.launchpad.net/keystoneauth/+bug/1502232
  * d/control: Add git to build-depends for sphinx-build
  * d/control: Add python-oslosphinx to build-depends.

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * d/p/remove-openstackdocstheme.patch: Drop openstackdocstheme from sphinx
    documentation.
  * d/control: Drop python-openstackdocstheme from BDs.

  [ Thomas Goirand ]
  * Standards-Version: 3.9.7 (no change).
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 04:38:29 +0000

python-openstacksdk (0.7.1-2) experimental; urgency=medium

  [ Thomas Goirand ]
  * Removed intersphinx module.

  [ Corey Bryant ]
  * d/rules: Run py3 tests.
  * d/watch: Updated to get tarball from github.
  * d/control: Add python(3)-keystoneauth1 to BDs.

 -- Corey Bryant <corey.bryant@canonical.com>  Wed, 16 Dec 2015 09:19:48 -0500

python-openstacksdk (0.7.1-1) experimental; urgency=medium

  * Initial release. (Closes: #778645)

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Feb 2015 21:48:45 +0100
